"use strict";

function changeName(name) {
    return name.charAt(0).toUpperCase() + name.substr(1).toLowerCase();
}

function modifyUrl(url) {
    return "http://" + url;
}

function modifyDescription(description) {
    return description.substr(0, 15) + "...";
}

function modifyDate(date) {
    var tmpDate = new Date(date);
    var year = tmpDate.getFullYear(),
        month = tmpDate.getMonth() + 1,
        day = tmpDate.getDate(),
        hours = tmpDate.getHours(),
        minutes = tmpDate.getMinutes();
    var addZero = function(partDate) {
        if (partDate < 10) {
            return "0" + partDate;
        } else {
            return partDate;
        }
    }
    month = addZero(month);
    day = addZero(day);
    hours = addZero(hours);
    minutes = addZero(minutes);
    return `${year}/${month}/${day} ${hours}:${minutes}`
}

function getChangedNewData(newData) {
    return newData.map(function(element, index) {
        return {
            name: changeName(element.name),
            url: modifyUrl(element.url),
            description: modifyDescription(element.description),
            date: modifyDate(element.date),
            params: `${element.params.status}=>${element.params.progress}`
        }
    });
}

function insertWithReplaceMethod(item, firstLine) {
    var resultHTML = "";
    var itemTemplate = '<div class="col-sm-3 col-xs-6">\
                <img src="$url" alt="$name" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">$name</div>\
                    <div class="text-muted">$description</div>\
                    <div class="text-muted">$params</div>\
                    <div class="text-muted">$date</div>\
                </div>\
            </div>';

    resultHTML = itemTemplate
        .replace(/\$name/gi, item.name)
        .replace("$url", item.url)
        .replace("$params", item.params)
        .replace("$description", item.description)
        .replace("$date", item.date);

    firstLine.innerHTML += resultHTML;

}

function insertWithInterpolationMethod(item, secondLine) {
    var itemTemplate = `<div class="col-sm-3 col-xs-6">\
                <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">${item.name}</div>\
                    <div class="text-muted">${item.description}</div>\
                    <div class="text-muted">${item.params}</div>\
                    <div class="text-muted">${item.date}</div>\
                </div>\
            </div>`;
    secondLine.innerHTML += itemTemplate;
}

function insertWithCreateElemMethod(item, thirdLine) {
    var elemDiv = document.createElement("div");
    elemDiv.className = "col-sm-3 col-xs-6";
    thirdLine.appendChild(elemDiv);

    var elemImg = document.createElement("img");
    elemImg.src = item.url;
    elemImg.className = "img-thumbnail";
    elemImg.alt = item.name;
    elemDiv.appendChild(elemImg);

    var divDescription = document.createElement('div');
    divDescription.className = "info-wrapper";
    elemDiv.appendChild(divDescription);

    var divItems = document.createElement('div');
    divItems.className = "text-muted";
    divDescription.appendChild(divItems);
    divItems.innerHTML = item.name;

    function cloneDiv(itemProperty) {
        var newEl = divItems.cloneNode(true);
        newEl.innerHTML = itemProperty;
        divDescription.appendChild(newEl);
    }
    cloneDiv(item.description);
    cloneDiv(item.params);
    cloneDiv(item.date);

}

(function transform() {
    var firstLine = document.querySelector('#first-line'),
        secondLine = document.querySelector('#second-line'),
        thirdLine = document.getElementById('third-line'),
        newData = getChangedNewData(data);

    newData.forEach(function(item, index) {
        if (index <= 2) {
            insertWithReplaceMethod(newData[index], firstLine);
        } else if (index <= 5) {
            insertWithInterpolationMethod(newData[index], secondLine);
        } else if (index < 9) {
            insertWithCreateElemMethod(newData[index], thirdLine);
        }
    });
})();
