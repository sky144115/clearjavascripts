//Домашняя работа по  уроку 4

var total = 0;
var textResult = document.getElementById("result");

for (let i = 1; i < 16; i++) {
    let first = Math.floor((Math.random() * 6) + 1);
    let second = Math.floor((Math.random() * 6) + 1);

    if( i == 8 || i == 13){continue};

    textResult.innerHTML += `Первая кость: ${first} -- Вторая кость: ${second}<br>`;

    if(first == second) {
        textResult.innerHTML += `Выпал дубль.Число ${first}<br>`;
    } else if(first <= 3 && second >= 3) {
        textResult.innerHTML += `Большой разброс между костями.
        Разница составляет ${second-first}<br>`;
    }

    total += first + second;

}
textResult.innerHTML += total > 100 ?  `Победа, Вы набрали ${total} очков!` :
      `Вы проиграли, у Вас ${total} очков`;
