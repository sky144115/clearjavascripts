/*  1.
    Создать главную функцию run() в которой будет выполняться основной код (цикл)
    Так же эта функция должна содержать в себе вызовы всех остальных функций.

    2.
    Сделать функцию для получения случайных чисел (getRndNumber).
    Значение каждой переменной, в которую мы записываем, 
    какая выпала кость получать с помощью вызова этой функции

    3.
    Сделать одну функцию которая будет печатать строки (print). 
    Она должна принимать только один аргумент. Строку текста которую надо напечатать.
    (если у вас выводите данные не только в div с id result а возможно еще в какой то другой div, 
    тогда функция должна принимать 2 аргумента: id и Строку)

    4.
    Сделать функцию для определения совпадений. (isNumbersEqual). 
    Она должна содержать в себе проверку на совпадение и внутри себя 
    вызывать функцию для печать данных в HTML (print)

    5.
    Сделать функцию для определения разницы. (isBigDifference). 
    Она должна содержать в себе соответствующую проверку и внутри себя 
    вызывать функцию для печать данных в HTML (print)

    6.
    Сделать функцию для вычисления результата total. 
    Функция должна вычислить результат и вернуть его. 
    То есть вернуть строку. Полученное из функции значение необходимо потом напечатать 
    с помощью функции (print)
*/

"use strict";


var resultElement = document.getElementById("result");


//функция для получения случайных чисел
function getRndNumber(){
    return Math.floor((Math.random()*6) + 1);
};

//функция печати строки
function print(text){
	resultElement.innerHTML += text;
}

// функция для определения совпадений
function isNumbersEqual(first, second){
	if(first==second){
		var number = second;
		resultElement.innerHTML += `Выпал дубль. Число ${number} <br>`;
	};
}

// функцию для определения разницы	
function isBigDifference(first, second){
	if(first < 3 && second > 4){
		var interval = second - first;
		resultElement.innerHTML += `Большой разброс между костями. Разница составляет ${interval} <br>`;
	};
} 

//функцию для вычисления результата
function getTotal(total){
	var res = total > 100 ? "Победа, вы набрали " + total + " очей " : "Вы проиграли, у вас " + total + " очей";
	return res;
}

function run(){
   var total = 0;
	for (var i = 1; i <= 15; i++){
        if (i == 8 || i == 13){continue};
			var first = getRndNumber(); 
			var second = getRndNumber();
			total += first + second;
			print(`Первая кость: ${first} Вторая кость: ${second}<br>`);
			isNumbersEqual(first, second);
			isBigDifference(first, second);
    };
	print(getTotal(total));	
};
run();

