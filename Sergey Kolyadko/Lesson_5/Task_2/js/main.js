var btn = document.getElementById("play");
var player1 = document.getElementById("player1");
var player2 = document.getElementById("player2");
var resultElement = document.getElementById("result");


function getPlayerResult() { return Math.floor((Math.random() * 3) + 1); }

function getNameById(number){

		if (number == 1){ 
			return "Камень"; 
		}
		if (number == 2){ 
			return "Ножницы"; 
		}
		if (number == 3){ 
			return "Бумага"; 
		}
}

function determineWinner(player1, player2){

	if (player1 == player2){
		return 0;
	}
	if (player1 == 1 && player2 == 2 || player1 == 3 && player2 == 1 || player1 == 2 && player2 == 3){
		return 1;
	}
	if (player1 == 1 && player2 == 3 || player1 == 2 && player2 == 1 || player1 == 3 && player2 == 2 ){
		return 2;
	}
}

function printResult(result){
    switch (result) {
        case 1:
           return "Выиграл первый игрок";
           break;    
        case 2:
           return "Выиграл второй игрок";
           break;
        case 0:
           return "Ничья";
           break;
    }   
}


function runGame() {
    var playerFirst = getPlayerResult();
	var playerSecond = getPlayerResult();
	
	player1.innerHTML = getNameById(playerFirst);
    player2.innerHTML = getNameById(playerSecond);
	
	resultElement.innerHTML = printResult(determineWinner(playerFirst, playerSecond));
	
}

btn.addEventListener("click", runGame);
