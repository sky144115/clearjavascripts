
var btn = document.getElementById("play");

function createFinalArray(primaryArray, count) {
    var newArray = [];
    primaryArray.forEach(function (item, index){
        if(count - 1 >= index){
            newArray.push({
                url         : item.url,
                name        : item.name,
                params      : item.params,
                description : item.description,
                date        : item.date
            })
        }
    })
    return newArray;
}

function editFinalArray(editedArray) {
    return editedArray.map(function (item, index) {
        item.url         = newUrl(item.url);
        item.name        = newName(item.name);
        item.params      = newParams(item.params);
        item.description = newDescription(item.description);
        item.date        = newDate(item.date);
    })
}

function newUrl(url) {
    return "http://" + url;
}

function newName(name) {
    return name[0].toLocaleUpperCase() + name.substring(1).toLowerCase();
}

function newParams(params) {
    return params.status + "=>" + params.progress;
}

function newDescription(description) {
    return description.substring(0, 15) + "...";
}

function newDate(dat){
    var tmpDate = new Date(dat);
    return fixDateString(tmpDate.getFullYear())     + "/" +
           fixDateString(tmpDate.getMonth() + 1)    + "/" +
           fixDateString(tmpDate.getDate())         + " " +
           fixDateString(tmpDate.getHours())        + ":" +
           fixDateString(tmpDate.getMinutes());
}

function fixDateString(start) {
    return (start < 10) ? "0" + start : start;
}

function printFinalArray(input) {
    console.log(input);
}

function transform() {
    var finalArray = createFinalArray(data,7)
    editFinalArray(finalArray);
    printFinalArray(finalArray);
}

btn.addEventListener("click", transform);
