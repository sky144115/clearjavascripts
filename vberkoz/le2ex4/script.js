//Lesson 2, exercise 4 homework
//
//Declare a variable with let keyword and assign to it value "JavaScript".
//Declare a variable with var keyword and assign to it value "courses".
//Concut these variables with space between them and assign result to variable result.
//Output variable result to the console.

let javaScript = "JavaScript";
var courses = "courses";

var result = javaScript + " " + courses;

console.log(result);